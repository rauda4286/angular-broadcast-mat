export interface DialogData {
  text: string;
  code: string;
  url: string;
}

