import { Component, Inject } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-camera-windows',
  templateUrl: './camera-windows.component.html',
  styleUrls: ['./camera-windows.component.css'],
})

/**
 * @title Dialog Overview
 */
export class CameraWindowsComponent {
  selected = 'option2';
  text: string;
  code: string;
  url: string;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '600px',
      height: '600px',
      data: {text: this.text},
    });

    dialogRef.afterClosed().subscribe((result) => {
     this.text = result;
    });
  }
}
