import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CameraWindowsComponent } from './camera-windows.component';

describe('CameraWindowsComponent', () => {
  let component: CameraWindowsComponent;
  let fixture: ComponentFixture<CameraWindowsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CameraWindowsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraWindowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
