import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeetingScreenComponent } from './components/meeting-screen/meeting-screen.component';
import { CameraWindowsComponent } from './components/camera-windows/camera-windows.component';

const routes: Routes = [
  { path: 'CameraWindowsComponent', component: CameraWindowsComponent },
  { path: 'MeetingScreenComponent', component: MeetingScreenComponent },
  { path: '**', redirectTo: 'MeetingScreenComponent' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
